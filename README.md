Slack Emoji Notifier
--------

# Operation

### Install

```
yarn install
```

### Check cloudformation template

```
$ yarn run build
$ cat .cloudformation/cloudformation-template-update-stack.json | jq . -C | less 
```

### Check artifact

```
$ npx sls webpack
$ less .built/...
```

### Release

```
$ yarn run test
$ TOKEN=XXXXXXXX HOOK_URL=YYYYYYYY CHANNEL=#ZZZZZZZZ yarn run deploy
```

* when initial deployment, should create `emoji.json` like `{emoji:[], cache_ts:null} in the bucket`
