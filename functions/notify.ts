import { ScheduledEvent, Handler, Context, Callback } from 'aws-lambda'
import { S3 } from 'aws-sdk'
import axios, { AxiosResponse } from 'axios'

interface SlackEmojiList {
  ok: string;
  emoji: Record<string, string>;
  cache_ts: string;
}

interface CachedData {
  emoji: string[]
  cache_ts: string;
}

const s3 = new S3({apiVersion: '2006-03-01'})
const params = {
  Bucket: 'newemoji-production',
  Key: 'emoji.json'
}

async function fetchEmoji(): Promise<AxiosResponse<SlackEmojiList>> {
  const url = `https://slack.com/api/emoji.list?token=${process.env.TOKEN}`
  const res = await axios.get(url)
  return res
}

async function postToSlack(newEmojis: string[]) {
  const url = process.env.HOOK_URL
  const message = `NEW EMOJI HERE!! -> ${newEmojis.map(_ => `:${_}:(\`${_}\`)`).join(', ')}`
  const payload = {
    'channel': process.env.CHANNEL,
    'username': 'New emoji releases',
    'icon_emoji': ':new:',
    'text': message
  }
  // :は\:にエスケープする必要がある
  const options = JSON.stringify(payload).replace(/':'/g, '\'\:\'')
  await axios.post(url!, options)
}

export async function notify(
    event: ScheduledEvent,
    context: Context,
    callback: Callback): Promise<void> {
  try {
    const fetched = await fetchEmoji()
    const latestCacheTs = fetched.data.cache_ts
    const latestEmojis: string[] = Object.keys(fetched.data.emoji)
    const cachedData: CachedData = JSON.parse(
      (await s3.getObject(params).promise()).Body!.toString())
    const cachedChacheTs = cachedData.cache_ts
    const cachedEmojis = cachedData.emoji
    // 2018/08/29から cache_ts=1517599539.000000のまま変更されてなくなっている
    // if (latestCacheTs == cachedChacheTs) {console.log(latestCacheTs);return}
    const diff = latestEmojis.filter(_ => (cachedEmojis).indexOf(_) === -1)
    if (diff.length > 0) {
      await postToSlack(diff)
      const newCache = {
        emoji: latestEmojis,
        cache_ts: latestCacheTs
      }
      await s3.putObject(Object.assign(params, {Body: JSON.stringify(newCache)})).promise()
    }
  } catch (error) {
    console.error(error);
  }
};
