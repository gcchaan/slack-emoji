import { Refs, S3 } from 'cloudform'

export const data = {
  NewEmojiS3: new S3.Bucket({
    BucketName: Refs.StackName,
  })
};
